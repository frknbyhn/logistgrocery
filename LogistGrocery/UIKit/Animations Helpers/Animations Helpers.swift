import Foundation
import UIKit

class AnimationsHelpers: NSObject{
    
    class func jumpView(view:UIView, duration:Double) {
        UIView.animate(withDuration: duration) {
            view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    class func imageTransition(imageVİew:UIImageView, name:String, duration:Double){
        UIView.transition(with: imageVİew, duration: duration, options: .transitionCrossDissolve, animations: {
            imageVİew.image = UIImage(named: name)
        }, completion: nil)
    }
    
    class func buttonImageTransition(button:UIButton, name:String, duration:Double){
          UIView.transition(with: button, duration: duration, options: .transitionCrossDissolve, animations: {
            button.setBackgroundImage(UIImage(named: name), for: .normal)
          }, completion: nil)
      }
}

