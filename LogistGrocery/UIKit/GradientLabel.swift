//
//  LNGradientLabel.swift
//  Loven
//
//  Created by Stable Mobile 07 on 21.09.2019.
//  Copyright © 2019 Loven App. All rights reserved.
//

import Foundation
import UIKit

final class GradientLabel: UILabel {
    var gradientColors: [CGColor] = [#colorLiteral(red: 0.9098039216, green: 0.1215686275, blue: 0.1215686275, alpha: 1),#colorLiteral(red: 1, green: 0.3098039216, blue: 0.1411764706, alpha: 1)] {
        didSet {
            self.drawText(in: self.frame)
        }
    }
    
    override func drawText(in rect: CGRect) {
        if let gradientColor = drawGradientColor(in: rect, colors: gradientColors) {
            self.textColor = gradientColor
        }
        super.drawText(in: rect)
    }
    
    private func drawGradientColor(in rect: CGRect, colors: [CGColor]) -> UIColor? {
        let currentContext = UIGraphicsGetCurrentContext()
        currentContext?.saveGState()
        defer { currentContext?.restoreGState() }
        
        let size = rect.size
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(),
                                        colors: colors as CFArray,
                                        locations: nil) else { return nil }
        
        let context = UIGraphicsGetCurrentContext()
        context?.drawLinearGradient(gradient,
                                    start: CGPoint.zero,
                                    end: CGPoint(x: size.width, y: 0),
                                    options: [])
        let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let image = gradientImage else { return nil }
        return UIColor(patternImage: image)
    }
}
