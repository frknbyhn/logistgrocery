//
//  CustomCartView.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 28.11.2020.
//

import UIKit

final class CustomCartView: UIView {
    
    
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var countLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.heightAnchor.constraint(equalToConstant: 45).isActive = true
    }
    
}
