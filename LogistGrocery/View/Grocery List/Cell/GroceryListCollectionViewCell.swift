//
//  GroceryListCollectionViewCell.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 28.11.2020.
//

import UIKit
import SDWebImage
import SwiftyAttributes
class GroceryListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mainView: ShadowView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var productInfo: UILabel!
    @IBOutlet weak var productQuantity: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var firstAddButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        firstAddButton.clipsToBounds = true
        firstAddButton.layer.cornerRadius = 4
        firstAddButton.layer.borderWidth = 1
        firstAddButton.layer.borderColor = #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 0.3)
        
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 4
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 0.3)
        
        countView.clipsToBounds = true
        countView.layer.cornerRadius = 12.5
        countView.layer.borderWidth = 1
        countView.layer.borderColor = #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 0.3)
    }
    
}
