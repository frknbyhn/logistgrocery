//
//  GroceryListHeader.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 28.11.2020.
//

import UIKit

class GroceryListHeader: UICollectionReusableView {
        
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var logo: UIImageView!
    
}
