//
//  BasketTableViewCell.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 29.11.2020.
//

import UIKit

class BasketTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productQuantity: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectedBackgroundView = UIView()
        productImage.clipsToBounds = true
        productImage.layer.cornerRadius = 4
        
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 22
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
        
        countView.clipsToBounds = true
        countView.layer.cornerRadius = 11
        countView.layer.borderWidth = 1
        countView.layer.borderColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
    }
    
}
