
import UIKit
import Foundation
import Lottie
import CoreLocation

struct ScreenSize {
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPHONE_XR         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
}

class AppUtility: NSObject {
    
    class func calculateUIBarButtonSize(font:UIFont,text: String) -> CGSize {
             
        let font = font
        let fontAttributes = [NSAttributedString.Key.font: font]
        return (text as NSString).size(withAttributes: fontAttributes)
    }
    
    class func drawGradientColor(in rect: CGRect, colors: [CGColor]) -> UIColor? {
          let currentContext = UIGraphicsGetCurrentContext()
          currentContext?.saveGState()
          defer { currentContext?.restoreGState() }
          let size = rect.size
          UIGraphicsBeginImageContextWithOptions(size, false, 0)
          guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(),
                                          colors: colors as CFArray,
                                          locations: nil) else { return nil }
          let context = UIGraphicsGetCurrentContext()
          context?.drawLinearGradient(gradient, 
                                      start: CGPoint.zero,
                                      end: CGPoint(x: size.width, y: 0),
                                      options: [])
          let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
          UIGraphicsEndImageContext()
          guard let image = gradientImage else { return nil }
          return UIColor(patternImage: image)
    }
    
    class func titleLabel(_ title: String) -> UILabel {
        let label  = UILabel()
        label.text = title
        label.textColor = AppUtility.UIColorFromRGB(0x0A2463)
        label.font = UIFont(name: "Poppins-Medium", size: 16.0)
        label.textAlignment = .center
        label.sizeToFit()
        return label
    }
    
    class func isRegisteredForRemoteNotifications() -> Bool {
        if #available(iOS 10.0, *) {
            var isRegistered = false
            let semaphore = DispatchSemaphore(value: 0)
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { settings in
                if settings.authorizationStatus != .authorized {
                    isRegistered = false
                } else {
                    isRegistered = true
                }
                semaphore.signal()
            })
            _ = semaphore.wait(timeout: .now() + 5)
            return isRegistered
        } else {
            return UIApplication.shared.isRegisteredForRemoteNotifications
        }
    }
    
    class func getCurrentDate() -> String {
        let todaysDate = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let DateInFormat = dateFormatter.string(from: todaysDate as Date)
        
        return DateInFormat
    }
    
    class func getCurrentClock() -> String{
        
        let date = Date()
        
        let hour   = Calendar.current.component(.hour, from: date)
        let minute = Calendar.current.component(.minute, from: date)
        
        print("hourrr", hour)
        
        var clock = ""

        if hour < 10 {
            clock =  "0" + String(hour) +  ":" + String(minute)
        }else {
            clock = String(hour) + ":" + String(minute)
        }
        
        if minute < 10{
             clock = String(hour) + ":0" + String(minute)
        }else{
             clock = String(hour) + ":" + String(minute)
        }
        
        return clock
    }
    
    class func getCurrentSecond() -> String {
        let date   = Date()
        let second = Calendar.current.component(.second, from: date)

        var seconds = ""
        
        if second < 10 {
            seconds = "0" + String(second)
        }else{
            seconds = String(second)
        }
        
        return seconds
    }
    
    class func isLocationServiceEnabled() -> String {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                return "notAllowed"
            case .authorizedWhenInUse:
                return "whenUsing"
            case .authorizedAlways:
                return "always"
            default:
                return "notAllowed"
            }
        }else{
            return "notAllowed"
        }
    }
    
    class func isLocationServicesEnabled() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            default:
                return false
            }
        }else{
            return false
        }
    }
    
    class func imageViewImageTransition(imageView: UIImageView, imageName: String) {
        UIView.transition(with: imageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            imageView.image = UIImage(named: imageName)
        }, completion: nil)
    }
    
    class func buttonImageTransition(button: UIButton, imageName: String) {
        UIView.transition(with: button, duration: 0.3, options: .transitionCrossDissolve, animations: {
            button.setImage(UIImage(named: imageName), for: .normal)
        }, completion: nil)
    }
    
    class func labelTransition(label: UILabel, labelText: String) {
        UIView.transition(with: label, duration: 0.3, options: .transitionCrossDissolve, animations: {
            label.text = labelText
        }, completion: nil)
    }
    
    class func detectFaces(image: UIImage) -> Bool {
        let faceImage = CIImage(image: image)
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: [CIDetectorAccuracy: CIDetectorAccuracyHigh])
        let faces = faceDetector?.features(in: faceImage!) as? [CIFaceFeature]
        print("Number of faces: \(faces?.count)")
        
        if faces?.count ?? 0 > 0 {
            return true
        } else {
            return false
        }
    }
    
    class func setAnimation(animationName: String, loop: Bool, myView: UIView) {
        let animationView = AnimationView(name: animationName)
        animationView.contentMode = .scaleAspectFill
        if !loop {
            animationView.loopMode = .playOnce
        } else {
            animationView.loopMode = .loop
        }
        animationView.tag = 100
        myView.addSubview(animationView)
        animationView.play()
    }
    
    class func createAnimationView(animationName: String, loop: Bool) -> AnimationView {
        let animationView = AnimationView(name: animationName)
        animationView.contentMode = .scaleAspectFit
        if !loop {
            animationView.loopMode = .playOnce
        } else {
            animationView.loopMode = .loop
        }
        return animationView
    }
    
    class func radiusView(view: UIView) {
        view.clipsToBounds = true
        view.layer.cornerRadius = view.frame.height / 2
        
        view.layer.borderColor = AppUtility.UIColorFromRGB(0x4D4BEC).cgColor
        view.layer.borderWidth = 4
    }
    
    class func jumpView(view: UIView) {
        UIView.animate(withDuration: 0.40) {
            view.transform = CGAffineTransform(scaleX: 2, y: 2)
            view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    class func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func nslocalized(_ key: String) -> String {
        return NSLocalizedString(key, comment: key)
    }
    
    class func titleLabel(_ title: String, color: UIColor) -> UILabel {
        let label  = UILabel()
        label.text = title
        label.textColor = color//AppUtility.UIColorFromRGB(0x828282).withAlphaComponent(0.9)
        label.font = UIFont(name: "Avenir-Heavy", size: 16.0)
        label.textAlignment = .center
        label.sizeToFit()
        return label
    }
    
    class func goURL(url: String) {
        guard let url = URL(string: url) else {
            return
        }
        UIApplication.shared.openURL(url)
    }
    
    class func alert(_ title: String, _ message: String) {
        let alert = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: String.localize(word: "okay"))
        alert.alertViewStyle = .default
        alert.show()
    }
}

public extension UIDevice {
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "7"
            case "iPhone5,3", "iPhone5,4":                  return "7"
            case "iPhone6,1", "iPhone6,2":                  return "7"
            case "iPhone7,2":                               return "7"
            case "iPhone7,1":                               return "7"
            case "iPhone8,1":                               return "7"
            case "iPhone8,2":                               return "7"
            case "iPhone8,4":                               return "7"
            case "iPhone9,1", "iPhone9,3":                  return "7"
            case "iPhone9,2", "iPhone9,4":                  return "78plus"
            case "iPhone10,1", "iPhone10,4":                return "78plus"
            case "iPhone10,2", "iPhone10,5":                return "78plus"
            case "iPhone10,3", "iPhone10,6":                return "x"
            case "iPhone11,2":                              return "x"
            case "iPhone11,4", "iPhone11,6":                return "x"
            case "iPhone11,8":                              return "xr"
            case "iPhone12,1":                              return "11"
            case "iPhone12,3":                              return "11pro"
            case "iPhone12,5":                              return "11pro"
            case "iPhone12,8":                              return "7"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch) (1st generation)"
            case "iPad8,9", "iPad8,10":                     return "iPad Pro (11-inch) (2nd generation)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch) (1st generation)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "iPad8,11", "iPad8,12":                    return "iPad Pro (12.9-inch) (4th generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }

        return mapToDevice(identifier: identifier)
    } ()
}
