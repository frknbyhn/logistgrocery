
import Foundation
import SwiftyJSON

final class JsonHelper {
    
    static func getJsonFile(jsonName: String, completion: @escaping (JSON?) -> Void) {
        if let path = Bundle.main.path(forResource: jsonName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                let json = JSON(jsonResult)
                
                completion(json)
            } catch {
                // handle error
                fatalError("JSON Not Found!")
            }
        } else {
            fatalError("Path Not Found!")
        }
    }
}
