
import Foundation
func NSDateTimeAgoLocalizedStrings(key: String) -> String {
    let bundlePath = Bundle.main.bundlePath
        + "/Frameworks/TimeAgo.framework/NSDateTimeAgo.bundle"
    guard let bundle = Bundle(path: bundlePath) else { return NSLocalizedString(key, comment: "") }
    
    return NSLocalizedString(key, tableName: "NSDateTimeAgo", bundle: bundle, comment: "")
}

func isInTheFuture(date: NSDate) -> Bool {
    if (date.compare(NSDate() as Date) == ComparisonResult.orderedDescending) {
        return true
    }
    return false
}

public extension NSDate {
    
    var timeAgo: String {
        
        if isInTheFuture(date: self) {
            return NSDateTimeAgoLocalizedStrings(key: String.localize(word: "Now"))
        }
        
        let now = NSDate()
        let seconds = Int(fabs(timeIntervalSince(now as Date)))
        let minutes = Int(round(Float(seconds) / 60.0))
        let hours = Int(round(Float(minutes) / 60.0))
        
        if seconds < 5 {
            return NSDateTimeAgoLocalizedStrings(key: String.localize(word: "Now"))
        } else if seconds < 60 {
            return stringFromFormat(format: "%%d%@\(String.localize(word: "SecondAgo"))", withValue: seconds)
        } else if seconds < 120 {
            return NSDateTimeAgoLocalizedStrings(key: "1 \(String.localize(word: "MinuteAgo"))")
        } else if minutes < 60 {
            return stringFromFormat(format: "%%d%@\(String.localize(word: "MinuteAgo"))", withValue: minutes)
        } else if minutes < 120 {
            return NSDateTimeAgoLocalizedStrings(key: "1 \(String.localize(word: "HourAgo"))")
        } else if hours < 24 {
            return stringFromFormat(format: "%%d%@\(String.localize(word: "HourAgo"))", withValue: hours)
        } else if hours < 24 * 7 {
            let formatter = DateFormatter()
            formatter.dateFormat = String(format: "EEEE '%@' HH:mm", NSDateTimeAgoLocalizedStrings(key: "-"))
            return formatter.string(from: self as Date)
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = String(format: "d MMM '%@' HH:mm", NSDateTimeAgoLocalizedStrings(key: "-"))
            return formatter.string(from: self as Date)
        }
    }
    
    var timeSince: String {
        
        if isInTheFuture(date: self) {
            return NSDateTimeAgoLocalizedStrings(key: String.localize(word: "Now"))
        }
        
        let now = NSDate()
        let seconds = Int(fabs(timeIntervalSince(now as Date)))
        let minutes = Int(round(Float(seconds) / 60.0))
        let hours = Int(round(Float(minutes) / 60.0))
        
        if seconds < 5 {
            return NSDateTimeAgoLocalizedStrings(key: String.localize(word: "Now"))
        } else if seconds < 60 {
            return stringFromFormat(format: "%%d %@sn", withValue: seconds)
        } else if minutes < 60 {
            return stringFromFormat(format: "%%d %@mn", withValue: minutes)
        } else if hours < 24 {
            return stringFromFormat(format: "%%d %@s", withValue: hours)
        } else if hours < 24 * 7 {
            let formatter = DateFormatter()
            formatter.dateFormat = String(format: "EE HH:mm")
            return formatter.string(from: self as Date)
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = String(format: "d MMM HH:mm")
            return formatter.string(from: self as Date)
        }
    }
    
    func stringFromFormat(format: String, withValue value: Int) -> String {
        let localeFormat = String(
            format: format,
            getLocaleFormatUnderscoresWithValue(value: Double(value)))
        
        return String(format: NSDateTimeAgoLocalizedStrings(key: localeFormat), value)
    }
    
    func getLocaleFormatUnderscoresWithValue(value: Double) -> String {
        let localeCode = NSLocale.preferredLanguages.first!
        
        if localeCode == "ru" {
            let XY = Int(floor(value)) % 100
            let Y = Int(floor(value)) % 10
            
            if Y == 0 || Y > 4 || (XY > 10 && XY < 15) {
                return ""
            }
            
            if Y > 1 && Y < 5 && (XY < 10 || XY > 20) {
                return "_"
            }
            
            if Y == 1 && XY != 11 {
                return "__"
            }
        }
        
        return ""
    }
}

