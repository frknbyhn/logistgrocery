
import UIKit

final class BaseNavigationController: UINavigationController {
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        if let lastVC = self.viewControllers.last
        {
            return lastVC.preferredStatusBarStyle
        }
        return .lightContent
    }
    
    override public var childForStatusBarStyle: UIViewController? {
        return self.topViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (UIApplication.shared.delegate as! AppDelegate).restrictRotation = .portrait
        setupUI()
    }
    
    fileprivate func setupUI() {
        /// -> Setup Navigation Bar
        view.backgroundColor = navigationBar.barTintColor
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage =  UIImage()
        navigationBar.barTintColor = .white
        navigationBar.tintColor = .white
    }
    
}

extension UINavigationController{
    
    func backToViewController(viewController: Swift.AnyClass) {
        for element in viewControllers as Array {
            if element.isKind(of: viewController) {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
    
}
