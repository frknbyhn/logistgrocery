//
//  SplashViewController.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 28.11.2020.
//

import UIKit

class SplashViewController: RootViewController {

    @IBOutlet weak var animView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setAnimation(view: animView, name: "splash", loop: true)
    }
        
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Helpers.delay(1) {
            self.getGroceryList()
        }
    }

    func getGroceryList() {
        GetListService.getList { (response) in
            guard let response = response else {
                return
            }
            if response.count != 0 {
                guard let vc = self.mainStoryboard.instantiateViewController(withIdentifier: "GroceryListViewController") as? GroceryListViewController else {
                    fatalError("GroceryListViewController not found")
                }
                vc.groceryList = response
                let nc = BaseNavigationController(rootViewController: vc)
                nc.modalPresentationStyle = .fullScreen
                vc.modalTransitionStyle = .coverVertical
                self.present(nc, animated: true, completion: nil)
            }
        }
        
    }
    
    
}
