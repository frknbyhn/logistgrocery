//
//  YesHandlerPopupViewController.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 29.11.2020.
//

import UIKit

class YesHandlerPopupViewController: RootViewController {

    @IBOutlet weak var effectView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainCenter: NSLayoutConstraint!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    var clearBasketProtocol : ClearBasketProtocol?
    var deleteItemProtocol : DeleteItemProtocol?
    
    var comingMain : String = ""
    var comingDesc : String = ""
    var comingButton : String = ""
    var sender : Int = 0
    var justOne : Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        self.mainCenter.constant = (UIScreen.main.bounds.height / 2 + self.mainView.bounds.height)
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showView()
    }
    
    func setupUI() {
        mainView.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.262745098, blue: 0.3098039216, alpha: 0.15)
        mainView.layer.borderWidth = 1
        self.blurSetup(view: effectView)
        radiusView(view: mainView, radius: 25)
        radiusView(view: deleteButton, radius: 16)
        radiusView(view: cancelButton, radius: 16)
        cancelButton.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.262745098, blue: 0.3098039216, alpha: 0.15)
        cancelButton.layer.borderWidth = 1
        mainLabel.text = String.localize(word: comingMain)
        descLabel.text = String.localize(word: comingDesc)
        cancelButton.setTitle(String.localize(word: "cancel"), for: .normal)
        deleteButton.setTitle(String.localize(word: comingButton), for: .normal)
        effectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeAction)))
    }
    
    @objc func showView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3) {
                self.mainCenter.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func hideView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3) {
                self.mainCenter.constant = (UIScreen.main.bounds.height / 2 + self.mainView.bounds.height)
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func closeAction() {
        hideView()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func deleteButtonClicked(_ sender: Any) {
        hideView()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.dismiss(animated: true) {
                if self.justOne {
                    self.deleteItemProtocol?.removeItemOnBasket(sender: self.sender)
                } else {
                    self.clearBasketProtocol?.clearBasketAction()
                }
            }
        }
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        closeAction()
    }
}

