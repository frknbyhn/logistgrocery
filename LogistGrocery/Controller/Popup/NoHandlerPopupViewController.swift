//
//  NoHandlerPopupViewController.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 28.11.2020.
//

import UIKit

class NoHandlerPopupViewController: RootViewController {
    
    @IBOutlet weak var effectView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainCenter: NSLayoutConstraint!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var okayButton: UIButton!
    
    var comingMain : String = ""
    var comingDesc : String = ""
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        self.mainCenter.constant = (UIScreen.main.bounds.height / 2 + self.mainView.bounds.height)
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showView()
    }
    
    func setupUI() {
        mainView.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.262745098, blue: 0.3098039216, alpha: 0.15)
        mainView.layer.borderWidth = 1
        self.blurSetup(view: effectView)
        radiusView(view: mainView, radius: 25)
        radiusView(view: okayButton, radius: 16)
        okayButton.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.262745098, blue: 0.3098039216, alpha: 0.15)
        okayButton.layer.borderWidth = 1
        mainLabel.text = String.localize(word: comingMain)
        descLabel.text = String.localize(word: comingDesc)
        okayButton.setTitle(String.localize(word: "okay"), for: .normal)
        effectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeAction)))
    }
    
    @objc func showView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3) {
                self.mainCenter.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func hideView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3) {
                self.mainCenter.constant = (UIScreen.main.bounds.height / 2 + self.mainView.bounds.height)
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func closeAction() {
        hideView()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func okayButtonClicked(_ sender: Any) {
        closeAction()
    }
    
}
