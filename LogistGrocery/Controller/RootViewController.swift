import UIKit
import Lottie
import AVFoundation
import SVProgressHUD
import ActiveLabel
import AVFoundation
import AVKit

class RootViewController: UIViewController, UIGestureRecognizerDelegate, UINavigationControllerDelegate {
    
    var animationView: AnimationView?
    
    var mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    var popupStoryboard : UIStoryboard = UIStoryboard(name: "Popup", bundle: nil)
    var authStoryboard : UIStoryboard = UIStoryboard(name: "Auth", bundle: nil)
//    var actionsStoryboard : UIStoryboard = UIStoryboard(name: "Actions", bundle: nil)
//    var preStoryboard : UIStoryboard = UIStoryboard(name: "Premium", bundle: nil)
//    var otherStoryboard : UIStoryboard = UIStoryboard(name: "Other", bundle: nil)
    
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer?
    
    var window: UIWindow?
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (UIApplication.shared.delegate as! AppDelegate).restrictRotation = .portrait
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        hideKeyboardWhenTappedAround()
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RootViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func blurSetup(view : UIView) {
        let blurView = UIVisualEffectView()
        blurView.frame = view.bounds
        blurView.backgroundColor = UIColor.black.withAlphaComponent(1)
        blurView.alpha = 0.6
        view.addSubview(blurView)
    }
    
    func checkCameraPermissions(completion: @escaping (Bool) -> Void) {
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        
        switch cameraAuthorizationStatus {
        case .denied:
            completion(false)
        case .notDetermined:
            // Prompting user for the permission to use the camera.
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                if granted {
                    completion(true)
                    print("Granted access to \(cameraMediaType)")
                } else {
                    completion(false)
                    print("Denied access to \(cameraMediaType)")
                }
            }
        default:
            completion(true)
            break
        }
    }
    
    @objc func setAnimation(view: UIView, name: String, loop: Bool) {
        if let animateTag = view.viewWithTag(100) {
            animateTag.removeFromSuperview()
        }
        animationView = AppUtility.createAnimationView(animationName: name, loop: loop)
        animationView?.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        animationView?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        animationView?.tag = 100
        
        self.animationView?.reloadImages()
        view.insertSubview(self.animationView!, at: 0)
        self.animationView?.play()
        self.animationView?.backgroundBehavior = .pauseAndRestore
        
        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: OperationQueue.main) { (_) in
            self.animationView?.play()
        }
    }
    
    func changeDateFormat(dateString: String, fromFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", toFormat: String) -> String {
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = fromFormat
//        inputDateFormatter.timeZone   = TimeZone(abbreviation: "UTC")
//        inputDateFormatter.locale = Locale.current
        let formattedDate = inputDateFormatter.date(from: dateString)
        
        let outputDateFormatter = DateFormatter()
        outputDateFormatter.dateFormat = toFormat
        return outputDateFormatter.string(from: formattedDate ?? Date())
    }
        
    func setPremium() {
        UserDefaults.standard.set(true, forKey: "Premium")
    }
    
    func unsetPremium(){
        UserDefaults.standard.set(false, forKey: "Premium")
    }
    
    func createActiveLabel(eulaLabel : ActiveLabel) {
        eulaLabel.textColor = #colorLiteral(red: 0.4784313725, green: 0.4784313725, blue: 0.4784313725, alpha: 0.6)
        let privacy = ActiveType.custom(pattern: String.localize(word: "privacyPolicy"))
        let eula    = ActiveType.custom(pattern: String.localize(word: "termsConditions"))
        eulaLabel.enabledTypes = [privacy,eula]
        eulaLabel.text = String.localize(word: "contract")
        eulaLabel.customColor[privacy] = #colorLiteral(red: 0.4784313725, green: 0.4784313725, blue: 0.4784313725, alpha: 1)
        eulaLabel.customColor[eula]    = #colorLiteral(red: 0.4784313725, green: 0.4784313725, blue: 0.4784313725, alpha: 1)
        eulaLabel.font = UIFont(name: "AvenirNext-Medium", size: 12)!
        eulaLabel.handleCustomTap(for: privacy) { element in
//            AppUtility.goURL(url: self.privacyURL)
        }
        eulaLabel.handleCustomTap(for: eula) { element in
//            AppUtility.goURL(url: self.eulaURL)
        }
    }
    
    func checkMail(email: String) -> Bool {
      let controlMail = email.replacingOccurrences(of: " ", with: "")
      let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
       
      let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
      return emailTest.evaluate(with: controlMail)
    }
    
    func radiusView(view : UIView, radius : CGFloat) {
        view.clipsToBounds = true
        view.layer.cornerRadius = radius
    }
    
    func scaleView(view : UIView) {
        UIView.animate(withDuration: 0.5, animations: {
            view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        },
        completion: { _ in
            UIView.animate(withDuration: 0.5) {
                view.transform = CGAffineTransform.identity
            }
        })
    }
    
    func openUrl(urlString: String) {
        guard let url = URL(string: urlString) else {
            return
        }

        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func openNoHandler(main : String, desc : String) {
        guard let vc = self.popupStoryboard.instantiateViewController(withIdentifier: "NoHandlerPopupViewController") as? NoHandlerPopupViewController else {
            fatalError("NoHandlerPopupViewController not found")
        }
        vc.comingMain = main
        vc.comingDesc = desc
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
}

extension RootViewController: AVPlayerViewControllerDelegate {
    
    func videoPlay(videoName: String, videoView: UIView) {
        let playerController = AVPlayerViewController()
        playerController.delegate = self
        let bundle = Bundle.main
        let moviePath: String? = bundle.path(forResource: videoName, ofType: "mp4")
        let movieURL = URL(fileURLWithPath: moviePath ?? "")
        player = AVPlayer(url: movieURL)
        player.isMuted = true
        
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerController.player = player
        self.addChild(playerController)
        videoView.addSubview(playerController.view)
        playerController.view.frame = videoView.frame
        playerController.showsPlaybackControls = false
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: nil) { (_) in
            self.player.seek(to: CMTime.zero)
            self.player.play()
        }
        player.play()
    }
    
}
