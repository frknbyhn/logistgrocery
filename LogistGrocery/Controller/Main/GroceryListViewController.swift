//
//  GroceryListViewController.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 28.11.2020.
//

import UIKit
import SDWebImage
import SwiftyAttributes

struct ItemsInBasket {
    static var basket = [ProductBasket]()
}

protocol AfterQuitBasketProtocol : class {
    func reloadList()
}

final class GroceryListViewController: RootViewController, AfterQuitBasketProtocol {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var groceryList = [GetListModel]()
    
    var header : GroceryListHeader?
    var headerHeight : CGFloat = 210
    
    var cartView : CustomCartView?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        self.setNeedsStatusBarAppearanceUpdate()
        addCartView()
        setupCollection()
    }
    
    func setupCollection() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "GroceryListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GroceryListCollectionViewCell")
        addStrechyHeader()
        updateHeaderView()
        collectionView.reloadData()
    }
    
    func addStrechyHeader() {
        let headerViewNIB = UINib(nibName: "GroceryListHeader", bundle: nil)
        guard let headerView =  headerViewNIB.instantiate(withOwner: nil, options: [:]).first as? GroceryListHeader else {
            fatalError("GroceryListHeader is not found!")
        }

        self.header = headerView
        
        collectionView.contentInset  = UIEdgeInsets(top: headerHeight + 10, left: 20, bottom: 40, right: 20)
        collectionView.contentOffset = CGPoint(x: 0, y: -headerHeight + 10)
        self.collectionView.addSubview(self.header ?? UIView())
    }

    func updateHeaderView() {
        var headerRect = CGRect(x: 0, y: -headerHeight, width: collectionView.bounds.width, height: headerHeight)
        if collectionView.contentOffset.y < -headerHeight {
            headerRect.origin.y    = collectionView.contentOffset.y
            headerRect.size.height = -collectionView.contentOffset.y
        }
        header?.frame = headerRect
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateHeaderView()
    }
    
    @objc func addCartView() {
        self.navigationItem.rightBarButtonItem = nil
        cartView = CustomCartView().loadNib() as? CustomCartView
        guard let cartView = cartView else { return }
        
        cartView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.cartAction)))
        self.radiusView(view: cartView.countView, radius: cartView.countView.bounds.width / 2)
        
        cartView.countLabel.text = "0"
        
        cartView.widthAnchor.constraint(equalToConstant: 90).isActive = true
        
        let cartBarButtonItem = UIBarButtonItem(customView: cartView)
        self.navigationItem.rightBarButtonItem = cartBarButtonItem
    }
    
    @objc func cartAction() {
        guard let vc = self.mainStoryboard.instantiateViewController(withIdentifier: "BasketViewController") as? BasketViewController else {
            fatalError("BasketViewController not found")
        }
        vc.afterQuitBasketProtocol = self
        let nc = BaseNavigationController(rootViewController: vc)
        nc.modalTransitionStyle = .coverVertical
        nc.modalPresentationStyle = .fullScreen
        self.present(nc, animated: true, completion: nil)
    }
    
    @objc func addBasketAction(_ sender : UIButton) {
        let product = self.groceryList[sender.tag]
        
        if !ItemsInBasket.basket.contains(where: { $0.id == product.id }) {
            let pro = ProductBasket(id: product.id, amount: 1, price: product.price, image: product.imageUrl, name: product.name, stock: product.stock, currency : product.currency)
            ItemsInBasket.basket.append(pro)
        } else {
            let basketProduct = ItemsInBasket.basket.filter({ $0.id == product.id }).first
            let amount = basketProduct?.amount ?? 0
            if amount < product.stock {
                var filtered = ItemsInBasket.basket.filter({ $0.id != product.id })
                let pro = ProductBasket(id: product.id, amount: amount + 1, price: product.price, image: product.imageUrl, name: product.name, stock: product.stock, currency : product.currency)
                filtered.append(pro)
                ItemsInBasket.basket = filtered
            } else {
                self.openNoHandler(main: "notEnoughProductMain", desc: "notEnoughProductDesc")
            }
        }
        self.reloadAmount()
        self.collectionView.reloadData()
    }
    
    @objc func removeBasketAction(_ sender : UIButton) {
        let product = self.groceryList[sender.tag]
        
        if ItemsInBasket.basket.contains(where: { $0.id == product.id }) {
            let basketProduct = ItemsInBasket.basket.filter({ $0.id == product.id }).first
            let amount = basketProduct?.amount ?? 0
            if amount > 0 {
                if amount - 1 == 0 {
                    let filtered = ItemsInBasket.basket.filter({ $0.id != product.id })
                    ItemsInBasket.basket = filtered
                } else {
                    var filtered = ItemsInBasket.basket.filter({ $0.id != product.id })
                    let pro = ProductBasket(id: product.id, amount: amount - 1, price: product.price, image: product.imageUrl, name: product.name, stock: product.stock, currency : product.currency)
                    filtered.append(pro)
                    ItemsInBasket.basket = filtered
                }
            }
        }
        self.reloadAmount()
        self.collectionView.reloadData()
    }
    
    @objc func firstAddAction(_ sender : UIButton) {
        let product = self.groceryList[sender.tag]
        if !ItemsInBasket.basket.contains(where: { $0.id == product.id }) {
            let pro = ProductBasket(id: product.id, amount: 1, price: product.price, image: product.imageUrl, name: product.name, stock: product.stock, currency : product.currency)
            ItemsInBasket.basket.append(pro)
            self.cartView?.countLabel.text = "\(1)"
        }
        self.reloadAmount()
        self.collectionView.reloadData()
    }
    
    func reloadAmount() {
        var basketAmount = Int()
        let group = DispatchGroup()
        ItemsInBasket.basket.forEach { (basket) in
            group.enter()
            basketAmount += basket.amount
            group.leave()
        }
        group.notify(queue: .main) {
            self.cartView?.countLabel.text = "\(basketAmount)"
        }
    }
    
    func reloadList() {
        reloadAmount()
        self.collectionView.reloadData()
    }
    
}

extension GroceryListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return groceryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroceryListCollectionViewCell", for: indexPath) as? GroceryListCollectionViewCell else {
            fatalError("GroceryListCollectionViewCell not found")
        }
        let product = self.groceryList[indexPath.row]
        cell.productImage.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        cell.productImage.contentMode = .scaleAspectFit
        cell.productImage.sd_setImage(with: URL(string: product.imageUrl), completed: nil)
        let name = "\(product.name) ".withTextColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)).withFont(UIFont(name: "Avenir-Medium", size: 17)!)
        let price = "\(product.price)".withTextColor(#colorLiteral(red: 1, green: 0.3137254902, blue: 0.3333333333, alpha: 1)).withFont(UIFont(name: "Avenir-Heavy", size: 17)!)
        let currency = " \(product.currency)".withTextColor(#colorLiteral(red: 1, green: 0.3137254902, blue: 0.3333333333, alpha: 1)).withFont(UIFont(name: "Avenir-Heavy", size: 17)!)
        cell.productInfo.attributedText = name + price + currency
        
        cell.addButton.tag = indexPath.row
        cell.removeButton.tag = indexPath.row
        cell.firstAddButton.tag = indexPath.row
        
        cell.addButton.addTarget(self, action: #selector(addBasketAction(_:)), for: .touchUpInside)
        cell.removeButton.addTarget(self, action: #selector(removeBasketAction(_:)), for: .touchUpInside)
        cell.firstAddButton.addTarget(self, action: #selector(firstAddAction(_:)), for: .touchUpInside)
        
        if ItemsInBasket.basket.contains(where: { $0.id == product.id }) {
            cell.countView.alpha = 1
            cell.firstAddButton.alpha = 0
            cell.productQuantity.backgroundColor = #colorLiteral(red: 1, green: 0.3137254902, blue: 0.3333333333, alpha: 1)
            cell.productQuantity.textColor = .white
        } else {
            cell.countView.alpha = 0
            cell.firstAddButton.alpha = 1
            cell.productQuantity.backgroundColor = .clear
            cell.productQuantity.textColor = .black
        }
        
        let filtered = ItemsInBasket.basket.filter({ $0.id == product.id }).first
        
        cell.productQuantity.text = "\(filtered?.amount ?? 0)"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width - 50
        return CGSize(width:  width / 2, height: (width / 2) + 100)
    }
    
}
