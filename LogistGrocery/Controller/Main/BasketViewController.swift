//
//  BasketViewController.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 28.11.2020.
//

import UIKit
import SwiftyJSON
import Alamofire

protocol ClearBasketProtocol : class {
    func clearBasketAction()
}

protocol DeleteItemProtocol : class {
    func removeItemOnBasket(sender:Int)
}

struct CheckoutBasket : Encodable {
    static var id = String()
    static var amount = Int()
}

final class BasketViewController: RootViewController, ClearBasketProtocol, DeleteItemProtocol {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private lazy var deleteCartButton: UIBarButtonItem = {
        let image = UIImage(named: "deleteCart")?.withRenderingMode(.alwaysOriginal)
        let button = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(deleteAction))
        button.tintColor = .white
        return button
    }()
    
    private lazy var closeButton: UIBarButtonItem = {
        let image = UIImage(named: "whiteClose")?.withRenderingMode(.alwaysOriginal)
        let button = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(closeAction))
        button.tintColor = .white
        return button
    }()
    
    var afterQuitBasketProtocol : AfterQuitBasketProtocol?
    
    var basketPrice = Double()
    var basketAmount : Int = 0
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var summaryView: UIView!
    @IBOutlet weak var checkoutView: UIView!
    @IBOutlet weak var checkoutLabel: UILabel!
    
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var noItemImage: UIImageView!
    @IBOutlet weak var noItemDesc: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        self.navigationItem.leftBarButtonItem = closeButton
        self.navigationItem.rightBarButtonItem = deleteCartButton
        self.setNeedsStatusBarAppearanceUpdate()
        checkoutView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(checkoutAction)))
        noItemDesc.text = String.localize(word: "noItemInBasket")
        radiusView(view: mainView, radius: 22)
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        radiusView(view: checkoutView, radius: 16)
        checkoutLabel.text = String.localize(word: "checkout")
        calculatePrice()
        setupTable()
    }
    
    func calculatePrice() {
        self.basketPrice = 0
        self.basketAmount = 0
        let group = DispatchGroup()
        ItemsInBasket.basket = ItemsInBasket.basket.sorted { (first, second) -> Bool in
            return first.price > second.price
        }
        ItemsInBasket.basket.forEach { (basket) in
            group.enter()
            self.basketPrice += Double(basket.amount) * basket.price
            self.basketAmount += basket.amount
            group.leave()
        }
        group.notify(queue: .main) {
            self.totalLabel.text = "\(String.localize(word: "totalItems")) \(self.basketAmount)"
            let price = String(format: "%.2f", self.basketPrice)
            self.priceLabel.text = "₺\(price)"
            self.tableView.reloadData()
            if ItemsInBasket.basket.count == 0 {
                self.noItemImage.alpha = 1
                self.noItemDesc.alpha = 1
            } else {
                self.noItemImage.alpha = 0
                self.noItemDesc.alpha = 0
            }
        }
    }

    @objc func addBasketAction(_ sender : UIButton) {
        let product = ItemsInBasket.basket[sender.tag]
        
        if !ItemsInBasket.basket.contains(where: { $0.id == product.id }) {
            let pro = ProductBasket(id: product.id, amount: 1, price: product.price, image: product.image, name: product.name, stock : product.stock, currency: product.currency)
            ItemsInBasket.basket.append(pro)
        } else {
            let basketProduct = ItemsInBasket.basket.filter({ $0.id == product.id }).first
            let amount = basketProduct?.amount ?? 0
            if amount < product.stock {
                var filtered = ItemsInBasket.basket.filter({ $0.id != product.id })
                let pro = ProductBasket(id: product.id, amount: amount + 1, price: product.price, image: product.image, name: product.name, stock : product.stock, currency: product.currency)
                filtered.append(pro)
                ItemsInBasket.basket = filtered
            }  else {
                self.openNoHandler(main: "notEnoughProductMain", desc: "notEnoughProductDesc")
            }
        }
        
        self.calculatePrice()
    }
    
    @objc func removeBasketAction(_ sender : UIButton) {
        let product = ItemsInBasket.basket[sender.tag]
        
        if ItemsInBasket.basket.contains(where: { $0.id == product.id }) {
            let basketProduct = ItemsInBasket.basket.filter({ $0.id == product.id }).first
            let amount = basketProduct?.amount ?? 0
            if amount > 0 {
                if amount - 1 == 0 {
                    guard let vc = self.popupStoryboard.instantiateViewController(withIdentifier: "YesHandlerPopupViewController") as? YesHandlerPopupViewController else {
                        fatalError("YesHandlerPopupViewController not found")
                    }
                    vc.justOne = true
                    vc.deleteItemProtocol = self
                    vc.sender = sender.tag
                    vc.comingMain = "clearItemMain"
                    vc.comingDesc = "clearItemDesc"
                    vc.comingButton = "clearBasketButton"
                    vc.clearBasketProtocol = self
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle = .crossDissolve
                    self.present(vc, animated: true, completion: nil)
                } else {
                    var filtered = ItemsInBasket.basket.filter({ $0.id != product.id })
                    let pro = ProductBasket(id: product.id, amount: amount - 1, price: product.price, image: product.image, name: product.name, stock : product.stock, currency: product.currency)
                    filtered.append(pro)
                    ItemsInBasket.basket = filtered
                }
            }
        }
        
        self.calculatePrice()
    }
    
    func removeItemOnBasket(sender:Int) {
        let product = ItemsInBasket.basket[sender]
        let filtered = ItemsInBasket.basket.filter({ $0.id != product.id })
        ItemsInBasket.basket = filtered
        self.calculatePrice()
    }
    
    func setupTable() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "BasketTableViewCell", bundle: nil), forCellReuseIdentifier: "BasketTableViewCell")
        tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        tableView.reloadData()
    }
    
    @objc func clearBasketAction() {
        ItemsInBasket.basket.removeAll()
        self.calculatePrice()
    }

    @objc func deleteAction() {
        if ItemsInBasket.basket.count == 0 {
            self.openNoHandler(main: "error", desc: "noItemInBasket")
        } else {
            guard let vc = self.popupStoryboard.instantiateViewController(withIdentifier: "YesHandlerPopupViewController") as? YesHandlerPopupViewController else {
                fatalError("YesHandlerPopupViewController not found")
            }
            vc.comingMain = "clearBasketMain"
            vc.comingDesc = "clearBasketDesc"
            vc.comingButton = "clearBasketButton"
            vc.clearBasketProtocol = self
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func closeAction() {
        self.dismiss(animated: true) {
            self.afterQuitBasketProtocol?.reloadList()
        }
    }
    
    @objc func checkoutAction() {
        if ItemsInBasket.basket.count == 0 {
            self.openNoHandler(main: "error", desc: "noItemInBasket")
        } else {
            var basket = [[String:Any]]()
            
            let group = DispatchGroup()
            
            ItemsInBasket.basket.forEach { (item) in
                group.enter()
                basket.append(["id" : item.id, "amount" : item.amount])
                group.leave()
            }
                        
            group.notify(queue: .main) {
                
                let parameter : [String : Any] = ["products" : basket]
                
                print("parameter is here", parameter)
                
                Alamofire.request("https://desolate-shelf-18786.herokuapp.com/checkout", method: .post, parameters: parameter, encoding: JSONEncoding.default).responseJSON { (response) in
                    switch response.result {
                    case .success(let result):
                        let json = JSON(result)
                        let response = CheckoutModel(withJSON: json)
                        print("response is here", json)
                        if response.message == "Satın alma başarılı" && response.orderID != "" {
                            self.clearBasketAction()
                            self.openNoHandler(main: "success", desc: "successWhenCheckout")
                        } else {
                            self.openNoHandler(main: "error", desc: "errorWhenCheckout")
                        }
                    default:
                        break
                    }
                }
            }
        }
    }

}

extension BasketViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ItemsInBasket.basket.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BasketTableViewCell", for: indexPath) as? BasketTableViewCell else {
            fatalError("BasketTableViewCell not found")
        }
        let pro = ItemsInBasket.basket[indexPath.row]
        
        cell.addButton.tag = indexPath.row
        cell.addButton.addTarget(self, action: #selector(addBasketAction(_:)), for: .touchUpInside)
        
        cell.removeButton.tag = indexPath.row
        cell.removeButton.addTarget(self, action: #selector(removeBasketAction(_:)), for: .touchUpInside)
        
        cell.productImage.sd_setImage(with: URL(string: pro.image), completed: nil)
        cell.productName.text = pro.name
        cell.productPrice.text = "\(pro.currency)\(pro.price)"
        cell.productQuantity.text = "\(pro.amount) \(String.localize(word: "piece"))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
}
