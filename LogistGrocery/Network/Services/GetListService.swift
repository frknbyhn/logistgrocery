//
//  GetListService.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 28.11.2020.
//

import Foundation

class GetListService {
    
    static func getList(completion:@escaping([GetListModel]?) -> Void) {
        
        let url = API.Feed.getList
        
        APIConnection.startRequest(url: url, parameters: [:], method: .get) { (response, error) in
            guard let response = response else {
                return
            }
            let list : [GetListModel] = response.arrayValue.map { dataJSON in
                return GetListModel(withJSON: dataJSON)
            }
            completion(list)
        }
        
    }
    
}
