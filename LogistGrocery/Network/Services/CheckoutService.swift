//
//  CheckoutService.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 29.11.2020.
//

import Foundation

class CheckoutService {
    
    static func send(parameter : [String : Any], completion:@escaping(CheckoutModel?) -> Void) {
        
        let url = API.Feed.checkout
        
        APIConnection.startRequest(url: url, parameters: parameter, method: .post) { (response, error) in
            guard let response = response else {
                return
            }
            let object = CheckoutModel(withJSON: response)
            completion(object)
        }
        
    }
    
}
