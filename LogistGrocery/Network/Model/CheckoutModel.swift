//
//  CheckoutModel.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 29.11.2020.
//

import Foundation
import SwiftyJSON

class CheckoutModel {
    
    var orderID : String
    var message : String
    
    init(withJSON JSON : JSON) {
        orderID = JSON["orderID"].stringValue
        message = JSON["message"].stringValue
    }
    
}
