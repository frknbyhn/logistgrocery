//
//  ProductBasket.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 28.11.2020.
//

import Foundation

class ProductBasket {
    
    var id : String
    var amount : Int
    var price : Double
    var image : String
    var name : String
    var stock : Int
    var currency : String
    
    init(id : String, amount : Int, price : Double, image : String, name : String, stock : Int, currency : String) {
        self.id = id
        self.amount = amount
        self.price = price
        self.image = image
        self.name = name
        self.stock = stock
        self.currency = currency
    }
    
}
