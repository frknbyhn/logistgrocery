//
//  GetListModel.swift
//  LogistGrocery
//
//  Created by Furkan Beyhan on 28.11.2020.
//

import Foundation
import SwiftyJSON

class GetListModel {
    
    var id : String
    var name : String
    var price : Double
    var currency : String
    var imageUrl : String
    var stock : Int
    
    init(withJSON JSON : JSON) {
        id = JSON["id"].stringValue
        name = JSON["name"].stringValue
        price = JSON["price"].doubleValue
        currency = JSON["currency"].stringValue
        imageUrl = JSON["imageUrl"].stringValue
        stock = JSON["stock"].intValue
    }
    
}

