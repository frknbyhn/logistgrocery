import Foundation
import SwiftyJSON
import Alamofire

enum API {
    
    fileprivate static let baseURL = "https://desolate-shelf-18786.herokuapp.com/"
    
    enum Feed {
        static let getList = baseURL + "list"
        static let checkout = baseURL + "checkout"
    }
}

class APIConnection {
    
    static func startRequest(url: String, parameters: [String: Any], method: HTTPMethod, isAuth: Bool = false, uploadedImage : UIImage = UIImage(), withImages: Bool = false, completion: @escaping (JSON?, Error?) -> Void) {
        print("urlll", url)
        
        var header: HTTPHeaders?
        
        if let token = UserDefaults.standard.string(forKey: "Token") {
            header = ["Authorization": "Bearer \(token)"]
        }
        
        if isAuth {
            header = [
                "deviceid": UserDefaults.standard.string(forKey: "UDID") ?? ""
            ]
        }
        
        if !withImages {
            Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
                switch response.result {
                case .success(let result):
                    let responseJSON = JSON(result)
                    print(responseJSON)
                    completion(responseJSON, nil)
                case .failure(let error):
                    print(error, header as Any, url)
                    completion(nil,error)
                }
            }
        } else {
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                let newImg = uploadedImage.jpegData(compressionQuality: 0.2)
                multipartFormData.append(newImg!, withName: "image", fileName:".jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                }
            }, usingThreshold:UInt64.init(),
               to: url,
               method: .post,
               headers: header,
               encodingCompletion: { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        print("something")
                    })
                    upload.responseJSON { response in
                        let json = JSON(response.result.value ?? (Any).self)
                        print("json is here", json)
                        completion(json, nil)
                    }
                    break
                case .failure(let encodingError):
                    print("the error is  : \(encodingError.localizedDescription)")
                    break
                }
            })
        }
    }

}
